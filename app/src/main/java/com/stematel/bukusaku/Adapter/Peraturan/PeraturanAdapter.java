package com.stematel.bukusaku.Adapter.Peraturan;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.stematel.bukusaku.Model.Peraturan.PasalResponse;
import com.stematel.bukusaku.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tofa-pc on 2/4/2018.
 */

public class PeraturanAdapter extends RecyclerView.Adapter<PeraturanHolder> implements Filterable {
    ArrayList<PasalResponse> mPasalList;
    ArrayList<PasalResponse> mFilteredList;
    private Context context;

    public PeraturanAdapter(ArrayList <PasalResponse> PasalList, Context context) {
        mPasalList = PasalList;
        mFilteredList = PasalList;
        this.context = context;
    }

    @Override
    public PeraturanHolder onCreateViewHolder (ViewGroup parent, int viewType){
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.pasal_list, parent, false);
        PeraturanHolder peraturanHolder = new PeraturanHolder(mView);
        return peraturanHolder;
    }

    @Override
    public void onBindViewHolder (final PeraturanHolder holder, final int position){

        PasalResponse pasal = mPasalList.get(position);

        holder.tvNo.setText(pasal.getNo());
        holder.tvJenis.setText(pasal.getJenis());
        holder.tvKategori.setText(pasal.getKategori());
        holder.tvKode.setText(pasal.getKode());
        holder.tvPoin.setText(pasal.getPoin());
        holder.tvKeterangan.setText(pasal.getKeterangan());

        holder.itemView.setTag(pasal);

    }

    @Override
    public int getItemCount () {
        return mFilteredList.size();
    }

    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                if (charString.isEmpty()) {

                    mFilteredList = mPasalList;
                } else {

                    ArrayList<PasalResponse> filteredList = new ArrayList<>();

                    for (PasalResponse pasalResponse : mPasalList) {

                        if (pasalResponse.getKeterangan().toLowerCase().contains(charString) || pasalResponse.getKategori().toLowerCase().contains(charString) || pasalResponse.getJenis().toLowerCase().contains(charString) || pasalResponse.getPoin().toLowerCase().contains(charString)){

                            filteredList.add(pasalResponse);
                        }
                    }

                    mFilteredList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mFilteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mFilteredList = (ArrayList<PasalResponse>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

}
