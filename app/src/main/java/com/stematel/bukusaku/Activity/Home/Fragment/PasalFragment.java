package com.stematel.bukusaku.Activity.Home.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.stematel.bukusaku.Adapter.Peraturan.PeraturanAdapter;
import com.stematel.bukusaku.Model.Peraturan.PasalResponse;
import com.stematel.bukusaku.Network.NetworkClient;
import com.stematel.bukusaku.Network.NetworkServices;
import com.stematel.bukusaku.R;
import com.stematel.bukusaku.Service.ResponseService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PasalFragment extends Fragment /*implements SearchView.OnQueryTextListener*/ {

    RecyclerView mRecyclerView;
    PeraturanAdapter mAdapter;
    private LinearLayoutManager mLayoutManager;

    EditText editText;

    public PasalFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
            View view = inflater.inflate(R.layout.fragment_pasal, container, false);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.pasallist);
        mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        editText = (EditText) view.findViewById(R.id.search_box);

        refresh();
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.my_option_menu, menu);

        final MenuItem item = menu.findItem(R.id.search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);

    }

    private void refresh() {
        Call<ResponseService<PasalResponse>> produkCall = NetworkClient.getInstance().getApiService().getPasal();
        produkCall.enqueue(new Callback<ResponseService<PasalResponse>>() {
            @Override
            public void onResponse(Call<ResponseService<PasalResponse>> call, Response<ResponseService<PasalResponse>> response) {
                ArrayList<PasalResponse> PasalList = response.body().getDatalist();
                mAdapter = new PeraturanAdapter(PasalList, getContext());
                mRecyclerView.setAdapter(mAdapter);
            }

            @Override
            public void onFailure(Call<ResponseService<PasalResponse>> call, Throwable t) {
                Log.e("Retrofit Get", t.toString());
            }
        });
    }

    /*private void search(SearchView searchView) {

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                mAdapter.getFilter().filter(newText);
                return true;
            }
        });*/

}
