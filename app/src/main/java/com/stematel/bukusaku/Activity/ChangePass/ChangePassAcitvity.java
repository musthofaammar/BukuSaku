package com.stematel.bukusaku.Activity.ChangePass;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.stematel.bukusaku.Activity.Home.HomeActivity;
import com.stematel.bukusaku.Activity.Login.LoginActivity;
import com.stematel.bukusaku.Model.User.UserResponse;
import com.stematel.bukusaku.R;
import com.stematel.bukusaku.Service.LocalService;
import com.stematel.bukusaku.Service.ResponseService;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ChangePassAcitvity extends AppCompatActivity implements ChangePassPresenter.ChangePassView{

    @BindView(R.id.EdGantiPass)
    EditText edgantipass;

    private ChangePassPresenter changePassPresenter;
    int status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_pass);
        ButterKnife.bind(this);

        changePassPresenter = new ChangePassPresenter();
        changePassPresenter.injectView(this);

        checklogin();
    }

    private void checklogin() {
        UserResponse user = LocalService.getLogin();
        if (user != null){
            status = user.getStatus();
            if (status == 1){
                startActivity(new Intent(getApplicationContext(), HomeActivity.class));
            }
        }
    }

    @OnClick(R.id.btngantipass)
    public void btngantipass(View view){
        changePassPresenter.postPassword(edgantipass.getText().toString());
    }

    @Override
    public void onLoading() {

    }

    @Override
    public void ondLoading() {

    }

    @Override
    public void onFailed(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSuccess(ResponseService<UserResponse> responseService) {
        Toast.makeText(this, responseService.getMessage(), Toast.LENGTH_SHORT).show();
        startActivity(new Intent(getApplicationContext(), LoginActivity.class));
    }
}
