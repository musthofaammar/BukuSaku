package com.stematel.bukusaku.Activity.Home;

import android.content.Intent;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabSelectListener;
import com.stematel.bukusaku.Activity.Home.Fragment.Home.HomeFragment;
import com.stematel.bukusaku.Activity.Home.Fragment.LaporFragment;
import com.stematel.bukusaku.Activity.Home.Fragment.NotifFragment;
import com.stematel.bukusaku.Activity.Home.Fragment.PasalFragment;
import com.stematel.bukusaku.Activity.Settings.SettingActivity;
import com.stematel.bukusaku.Adapter.Peraturan.PeraturanAdapter;
import com.stematel.bukusaku.R;

public class HomeActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    PeraturanAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BottomBar bottomBar = (BottomBar) findViewById(R.id.bottombar);
        bottomBar.setItems(R.xml.tab_menu);
        bottomBar.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelected(@IdRes int tabId) {
                if (tabId == R.id.tab_home){
                    commitFragment(new HomeFragment());
                } else if (tabId == R.id.tab_peraturan){
                    commitFragment(new PasalFragment());
                } else if (tabId == R.id.tab_lapor){
                    commitFragment(new LaporFragment());
                } else {
                    commitFragment(new NotifFragment());
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.my_option_menu, menu);

        final MenuItem item = menu.findItem(R.id.search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextListener(this);
        //getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId()==R.id.Settings){
            startActivity(new Intent(getApplicationContext(), SettingActivity.class));
            Toast.makeText(getApplicationContext(), "Settings", Toast.LENGTH_SHORT).show();
        }

        return true;
    }

    private void commitFragment(Fragment fragment){
        android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.contentContainer, fragment);
        fragmentTransaction.commit();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        mAdapter.getFilter().filter(newText);
        return true;
    }
}
