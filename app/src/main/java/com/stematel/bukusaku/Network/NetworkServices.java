package com.stematel.bukusaku.Network;

import com.stematel.bukusaku.Model.Pelanggaran.JumlahPelanggaran;
import com.stematel.bukusaku.Model.Pelanggaran.PelanggaranResponse;
import com.stematel.bukusaku.Model.Peraturan.PasalResponse;
import com.stematel.bukusaku.Model.Upload.FileResponse;
import com.stematel.bukusaku.Model.User.PasswordPost;
import com.stematel.bukusaku.Model.User.PelanggaranPost;
import com.stematel.bukusaku.Model.User.SigninPost;
import com.stematel.bukusaku.Model.User.UserResponse;
import com.stematel.bukusaku.Service.ResponseService;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by tofa-pc on 2/3/2018.
 */

public interface NetworkServices {

    @POST("login.php")
    Call<ResponseService<UserResponse>> postLogin (@Body SigninPost signinPost);

    @POST("changepass.php")
    Call<ResponseService<UserResponse>> postPassword (@Body PasswordPost passwordPost);

    @POST("pelanggaran/historipelanggaran.php")
    Call<ResponseService<PelanggaranResponse>> postPelanggaran (@Body PelanggaranPost pelanggaranPost);

    @POST("pelanggaran/historypenghargaan.php")
    Call<ResponseService<PelanggaranResponse>> postPenghargaan (@Body PelanggaranPost pelanggaranPost);

    @POST("pelanggaran/getjumlahpoin.php")
    Call<ResponseService<JumlahPelanggaran>> postJumlah (@Body PelanggaranPost pelanggaranPost);

    @GET("peraturan/getperaturan.php")
    Call<ResponseService<PasalResponse>> getPasal();

    @Multipart
    @POST("upload.php")
    Call<ResponseService<FileResponse>> uploadPhoto(@Part MultipartBody.Part file, @Part("name") RequestBody name, @Part("keterangan") RequestBody keterangan);

}
