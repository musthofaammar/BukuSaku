package com.stematel.bukusaku.Model.User;

import com.google.gson.annotations.SerializedName;

/**
 * Created by tofa-pc on 2/4/2018.
 */

public class PasswordPost {
    @SerializedName("password")
    String password;

    public PasswordPost(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
